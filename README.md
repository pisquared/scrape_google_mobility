# Scrapes the Google Mobility data

From [the official source](https://www.google.com/covid19/mobility/)

National data and county data works (at least on some pdfs)

Format is:

```angular
{
  'country_code': <Country Code>,
  'county_data': [{
      'county_name': <County Name>,
      'data': [
        {
          'category': <Category Name>, 
          'percent': <Percent int>
        },
        ...
      ],
  }],
  'national_data': [
    {
      'category': <Category Name>, 
      'percent': <Percent int>
    },
    ...
  ]
}
```


## To install:

Assumes existence of `pdftohtml` utility (`sudo apt install pdftohtml`)

```bash
virtualenv -p python3 venv
source venv/bin/activate
pip install -r requirements.txt
```

To run: 

```bash
python scrape.py <list of ISO 3166-2 country codes>
```

e.g. 

```bash
python scrape.py us it gb
```

To get US state, use the two letter abbreviation of the state prefixed with `-`, e.g. for Texas:

```bash
python scrape.py -tx
```

To get everything:

```bash
python scrape.py all
```