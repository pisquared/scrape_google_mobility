import os
import shutil
import subprocess
import sys
from pprint import pprint

from lxml import html
import requests
from lxml.cssselect import CSSSelector

from country_codes import STATES_MAPPING, COUNTRIES_MAPPING

DATE = "2020-03-29"
BASE_URL_FMT = "https://www.gstatic.com/covid19/mobility/{date}_{country_code}_Mobility_Report_en.pdf"

basepath = os.path.dirname(os.path.realpath(__file__))
DATA_DIR = os.path.join(basepath, "data")
MAKEDIRS = [DATA_DIR, ]

DEBUG = False


def init():
    for d in MAKEDIRS:
        os.makedirs(d, exist_ok=True)


def url_to_html(date, country_code):
    response = requests.get(BASE_URL_FMT.format(date=date,
                                                country_code=country_code))
    pdf_filepath = os.path.join(basepath, DATA_DIR,
                                "{}.pdf".format(country_code))
    if os.path.exists(pdf_filepath):
        if DEBUG:
            print("PDF file {}.pdf exists, skipping...".format(country_code))
    else:
        with open(pdf_filepath, 'wb') as f:
            if response.status_code != 200:
                raise Exception("Error {} downloading country code {}".format(response.status_code,
                                                                              country_code))
            f.write(response.content)

    html_filepath = os.path.join(basepath, DATA_DIR, "{}.html".format(country_code))
    if os.path.exists(html_filepath):
        if DEBUG:
            print("HTML file {}.html exists, skipping...".format(country_code))
    else:
        tmp_dir = os.path.join(basepath, DATA_DIR, country_code)
        os.makedirs(tmp_dir, exist_ok=True)
        tmp_pdf = os.path.join(tmp_dir, "{}.pdf".format(country_code))
        shutil.copy(pdf_filepath, tmp_pdf)
        process = subprocess.Popen(["pdftohtml", "-s", tmp_pdf], cwd=tmp_dir,
                                   stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        stdout, stderr = process.communicate()
        if stderr:
            print("PDFtoHTML stderr: {}".format(stderr))
        html_src = os.path.join(tmp_dir, "{}-html.html".format(country_code))
        os.rename(html_src, html_filepath)
        try:
            shutil.rmtree(tmp_dir)
        except OSError as e:
            print("Error: %s : %s" % (tmp_dir, e.strerror))


def parse_category(text):
    return text.strip().replace('\xa0', ' ')


def parse_percent(text):
    text = text.strip().replace('%', '')
    text = text.strip().replace('\xa0', ' ')
    return int(text.replace('compared to baseline', ''))


def parse_national_data(tree):
    category_classes, percent_classes = [], []

    sel = CSSSelector("#page1-div p")
    all_p = sel(tree)
    for i, el in enumerate(all_p):
        if el.text == 'Retail & recreation':
            category_classes.append(el.attrib.get('class'))
            percent_classes.append(all_p[i + 1].attrib.get('class'))

    sel = CSSSelector("#page2-div p")
    all_p = sel(tree)
    for i, el in enumerate(all_p):
        if el.text == 'Transit stations':
            category_classes.append(el.attrib.get('class'))
            percent_classes.append(all_p[i + 1].attrib.get('class'))

    cat_percents = {}
    for i in range(len(category_classes)):
        cat_class, percent_class = category_classes[i], percent_classes[i]
        cat_sel, percent_sel = CSSSelector("." + cat_class), CSSSelector("." + percent_class)
        cat, percent = cat_sel(tree), percent_sel(tree)
        cat_percents.update(dict(zip([parse_category(c.text) for c in cat],
                                     [parse_percent(p.text) for p in percent])))
    national_data = []
    for category, percent in cat_percents.items():
        national_data.append(dict(category=category,
                                  percent=percent))
    return national_data


def parse_county_data(tree):
    county_data = list()
    divs = CSSSelector("div")(tree)
    for page in range(3, len(divs)):
        elements = CSSSelector("#page{}-div > p".format(page))(tree)
        county_class = elements[0].attrib.get('class')
        category_class = elements[1].attrib.get('class')
        percent_class = elements[2].attrib.get('class')
        for p in elements:
            p_class = p.attrib.get('class')
            if p_class == county_class:
                text = parse_category(p.text)
                county_data.append(dict(
                    county_name=text,
                    data=[],
                ))
            elif p_class == category_class:
                text = parse_category(p.text)
                if 'Not enough data for this date' in text:
                    continue
                elif 'needs a significant volume' in text:
                    continue
                county_data[-1]["data"].append(dict(category=text))
            elif p_class == percent_class:
                if 'Not' in p.text:
                    del county_data[-1]["data"][-1]
                    continue
                county_data[-1]["data"][-1]["percent"] = parse_percent(p.text)
            else:
                continue
    return county_data


def parse_html(country_code):
    html_filepath = os.path.join(basepath, DATA_DIR, "{}.html".format(country_code))
    with open(html_filepath) as f:
        file_contents = f.read()
        cleaned_html_ar = []
        splitted_html = file_contents.split('\n')
        ignore, first_body = False, True
        for i, line in enumerate(splitted_html):
            if line == '</body>':
                ignore = True
            elif '<body ' in line:
                if not first_body:
                    ignore = False
                    continue
                if first_body:
                    first_body = False
            if not ignore:
                cleaned_html_ar.append(line)
        cleaned_html_ar += ["</body>", "</html>"]
        cleaned_html = '\n'.join(cleaned_html_ar)
    tree = html.fromstring(cleaned_html)

    national_data = parse_national_data(tree)
    county_data = parse_county_data(tree)
    country_name = COUNTRIES_MAPPING.get(country_code)
    return dict(country_code=country_code,
                country_name=country_name,
                national_data=national_data,
                county_data=county_data,
                )


def main(country_codes):
    for country_code in country_codes:
        if country_code.startswith('-'):
            assert len(country_code) == 3
            full_state_name = STATES_MAPPING.get(country_code[1:].upper())
            country_code = "US_{}".format(full_state_name.replace(' ', '_'))
        else:
            assert len(country_code) == 2
            country_code = country_code.upper()
        try:
            url_to_html(DATE, country_code)
            d = parse_html(country_code)
            pprint(d)
        except Exception as e:
            print(e)


if __name__ == "__main__":
    init()
    country_codes = sys.argv[1:]
    if len(country_codes) == 1 and country_codes[0] == 'all':
        country_codes = list(COUNTRIES_MAPPING.keys())
        country_codes += ["-{}".format(state) for state in STATES_MAPPING.keys()]
    main(country_codes)
